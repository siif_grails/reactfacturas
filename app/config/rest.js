var server = 'http://localhost:3000';
var camundaServer = 'http://127.0.0.1:8080';

module.exports = {
	urls: {
		facturas: server + '/facturas',
		unaFactura: server + '/facturas/:id',
		clientes: server + '/cliente',
		items: server + '/facturas/:id/items',
		clientesXCrear: camundaServer +'/engine-rest/task?taskDefinitionKey=:taskId&candidateGroup=:rol',
		taskVariables:  camundaServer +'/engine-rest/task/:taskInstanceId/variables',
		taskComplete:  camundaServer +'/engine-rest/task/:taskInstanceId/complete',
		facturasXCrear: camundaServer +'/engine-rest/task?taskDefinitionKey=:taskId&candidateGroup=:rol',

		login: server + '/login'
	}
}
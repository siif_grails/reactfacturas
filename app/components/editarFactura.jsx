import React from 'react'
import styles from '../styles/estilos.css';
import { Form, Icon, Input, Button } from 'antd';
import config from '../config/rest.js';

var FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

var EditarFactura = React.createClass({

  getInitialState: function() {
    return {datos:null, idFactura:"", clientes:[]}    
  },

  getData: function(idFactura) {
    //trae los registros de la base de datos
    var _this = this;
    fetch(config.urls.unaFactura.replace(":id", idFactura))
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      _this.props.form.setFieldsValue({
        numero: jsonObject.numero,
        documento: jsonObject.cliente.documento,
        fecha: jsonObject.fecha
      });

      _this.setState({
          datos: jsonObject,
          idFactura: _this.props.idFactura
      });

    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  getClientes: function() {
    //trae los registros de clientes
    var _this = this;
    fetch(config.urls.clientes)
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      /*llega un array de clientes*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                documento: item.documento, 
                nombre: item.nombre
                })
      });
      _this.setState({clientes: _data});

    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  componentWillReceiveProps: function(nextProps){
    if (this.props.idFactura !== nextProps.idFactura) this.getData(nextProps.idFactura);
  },

  componentDidMount: function() {
    console.log("Trigger Did mount!!!");
    this.props.form.validateFields();
    this.getData(this.props.idFactura);
    this.getClientes();
  },

  handleSubmit: function(e){
    e.preventDefault();
    var _this = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        _this.saveData(values, _this.props.parent.handleOk);
      }
    });
  },

  saveData: function(data, fnCallback){
    var _this = this;
    fetch(config.urls.unaFactura.replace(":id", _this.props.idFactura), 
          {   method:"PATCH",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
            }
          )
    .then(function(response) { 
      console.log("ok factura actualizada");
      return response.json()
    })
    .then(function(jsonObject) {
      fnCallback();
    })
    .catch(function(err) {
        console.log("error updating");
        console.log(err)
    }); 
  },

  render: function() {

    var _this = this;
    var _state = this.state;

    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const numFacturaError = isFieldTouched('numero') && getFieldError('numero');
    const clienteError = isFieldTouched('cliente') && getFieldError('cliente');
    const fechaError = isFieldTouched('fecha') && getFieldError('fecha');
    
    /*crea los option del select de clientes existentes*/
    var _clientes =""
    if (_state.clientes.length >0) {
      _clientes = _state.clientes.map(function(item,i) {
         return <option key={"cliente_" +i} value={item.documento} >{item.nombre}</option>
      });
    }

    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>

        <FormItem
          validateStatus={numFacturaError ? 'error' : ''}
          help={numFacturaError || ''}
        >
          {getFieldDecorator('numero', {
            rules: [{ required: true, message: 'Número de la factura' }],
          })(
            <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Número"/>
          )}
        </FormItem>

        <FormItem
          validateStatus={clienteError ? 'error' : ''}
          help={clienteError || ''}
        >
          {getFieldDecorator('documento', {
            rules: [{ required: true, message: 'Seleccione un cliente!' }],
          })(
            <select> 
                {_clientes}
            </select>
          )}
        </FormItem>

        <FormItem
          validateStatus={fechaError ? 'error' : ''}
          help={fechaError || ''}
        >
          {getFieldDecorator('fecha', {
            rules: [{ required: true, message: 'Fecha' }]
          })(
            <Input prefix={<Icon type="calendar" style={{ fontSize: 13 }} />} type="text" placeholder="Fecha"/>
          )}
        </FormItem>

        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            loading={this.props.parent.state.loading} onClick={this.props.parent.handleOk}
          >
            Guardar
          </Button>
        </FormItem>

      </Form>
    );

  }

});
const WrappedEditarFactura = Form.create()(EditarFactura);
module.exports = WrappedEditarFactura 

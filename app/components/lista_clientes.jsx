import React from 'react'
import styles from '../styles/estilos.css';

import { Table,Input } from 'antd';


import Encabezado from './encabezado';
import Detalles from './detalles';
import ListRow from './list_row';

import moment  from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import DateTimePicker  from 'react-widgets/lib/DateTimePicker';
import 'react-widgets/lib/less/react-widgets.less';

import configure from 'react-widgets/lib/configure';

import config from '../config/rest.js';
import { Button, Menu, Dropdown, Icon } from 'antd'

configure.setDateLocalizer(momentLocalizer(moment));


function formatCurrency(n, currency, scale) {
    return currency + " " + (n/scale).toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

var ListaClientes = React.createClass({
  getInitialState: function() {

    return {datos:[],modificar:false,idModificar:"",documento:"",nombre:"",telefono:"",email:"",direccion:""}    
  },

  getData: function() {
    //trae los registros de la base de datos
    var _this = this;
    console.log(config);    
    fetch(config.urls.clientes)
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      console.log(jsonObject)
      /*llega un array de facturas*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                documento: item.documento, 
                nombre: item.nombre,
                telefono: item.telefono,
                email: item.email,
                direccion: item.direccion
              })
      });
      _this.setState({datos: _data});


    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  componentDidMount: function() {
    this.getData();
  },
  modificar: function(e,DatosCliente) {
    var _state=this.state;
    var _modificar = !this.state.modificar;
    _state.documento=DatosCliente.documento;
    _state.nombre=DatosCliente.nombre;
    _state.telefono=DatosCliente.telefono;
    _state.email=DatosCliente.email;
    _state.direccion=DatosCliente.direccion;
    this.setState({modificar:_modificar});
    this.setState({idCliente:DatosCliente.id});
  },
  update: function(e,idCliente) {
    /*grabar los cambios del item*/
    var _this = this    
    fetch(config.urls.clientes+'/'+idCliente, 
          {   method:"PATCH",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({documento: this.state.documento,
                nombre: this.state.nombre,
                telefono: this.state.telefono,
                email: this.state.email,
                direccion: this.state.direccion})
          }
          )
    .then(function(response) { 
      console.log("ok updated");
      return response.json()
    })
    .then(function(jsonObject) {
      _this.setState({modificar: false});
      //_this.modificar(e,idCliente);
      _this.getData()
      console.log(jsonObject)
    })
    .catch(function(err) {
        console.log("error updating");
        console.log(err)
    });

  },
  documentoChange: function(e) {
    var _value = e.target.value;
    console.log(_value);
    this.setState({documento: _value});
  },
  nombreChange: function(e) {
    var _value = e.target.value;
    this.setState({nombre: _value});
  },
  telefonoChange: function(e) {
    var _value = e.target.value;
    this.setState({telefono: _value});
  },
  emailChange: function(e) {
    var _value = e.target.value;
    this.setState({email: _value});
  },
  direccionChange: function(e) {
    var _value = e.target.value;
    this.setState({direccion: _value});
  },
  render: function() {
    var _datos = this.state.datos;
    var _items = "";
    var _this = this;
    var _state = this.state
    var _modificar = this.state.modificar;
    var _idCliente=this.state.idCliente;
    console.log(this.state.documento);
    var columns = [{
          title: 'Documento',
          dataIndex: 'documento',
          key: 'documento',
          render: function(text, record){
            if (_modificar && record.id == _idCliente)
              return <input type="text" placeholder="documento" value={_state.documento} onChange={_this.documentoChange} />
            else
              return text}
        }, {
          title: 'Nombre',
          dataIndex: 'nombre',
          key: 'nombre',
          render: function(text,record){
            if (_modificar && record.id == _idCliente)
              return <input type="text" placeholder="nombre" value={_state.nombre} onChange={_this.nombreChange} />
            else
              return text}
        },{
          title: 'Telefono',
          dataIndex: 'telefono',
          key: 'telefono',
          render: function(text,record){
            if (_modificar && record.id == _idCliente)
              return <input type="text" placeholder="telefono" value={_state.telefono} onChange={_this.telefonoChange} />
            else
              return text}
        }, {
          title: 'Email',
          dataIndex: 'email',
          key: 'email',
          render: function(text,record){
            if (_modificar && record.id == _idCliente)
              return <input type="text" placeholder="email" value={_state.email} onChange={_this.emailChange} />
            else
              return text}
        }, {
          title: 'Direccion',
          dataIndex: 'direccion',
          key: 'direccion',
          render: function(text,record){
            if (_modificar && record.id == _idCliente)
              return <input type="text" placeholder="direccion" value={_state.direccion} onChange={_this.direccionChange} />
            else
              return text}
        }, {
          title: ' ',
          dataIndex: 'accion',
          key: 'accion',
          render: function(text,record) {
            if (_modificar && record.id == _idCliente)
              return <div><div><Button href="#" type="primary" onClick={(e)=>_this.modificar(e,record)}>cancelar</Button></div><div><Button href="#" type="primary" onClick={(e)=>_this.update(e,record.id)}>guardar</Button></div></div>
            else
              return <Button href="#" type="primary" onClick={(e)=>_this.modificar(e,record)}>modificar</Button>}
        }
        ];

    if (_datos.length > 0) {
      _items = <Table columns={columns} dataSource={_datos} />;
    }
    else {
      _items = <h2>cargando datos...</h2>
    }


    return <div>
            {_items}
          </div>
  }

});


module.exports = ListaClientes
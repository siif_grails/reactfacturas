import React from 'react'

var DetailRow =  React.createClass({
  getInitialState: function() {
    var _datos = this.props.data;
    return({modificar: false,
            itemNombre: _datos.item,
            cantidad:_datos.cantidad,
            valor: _datos.valor });
  },

  modificarItem: function() {
    this.setState({modificar: true})
  },

  itemNombreChange: function(e) {
    var _value = e.target.value;
    this.setState({itemNombre: _value});
  },
  cantidadChange: function(e) {
    var _value = e.target.value;
    this.setState({cantidad: _value});
  },
  valorChange: function(e) {
    var _value = e.target.value;
    this.setState({valor: _value});
  },

  cancel: function() {
    this.setState({modificar: false});
  },

  update: function() {
    /*grabar los cambios del item*/
    var _this = this    
    fetch('http://192.168.0.122:8080/items/'+ this.props.data.id, 
          {   method:"PUT",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({item: this.state.itemNombre,
                cantidad: this.state.cantidad,
                valor: this.state.valor})
          }
          )
    .then(function(response) { 
      console.log("ok updated");
      return response.json()
    })
    .then(function(jsonObject) {
      _this.setState({modificar: false});
      _this.props.parent.getData()
      console.log(jsonObject)
    })
    .catch(function(err) {
        console.log("error updating");
        console.log(err)
    }); 
  },

  render: function() {
    var _item = this.props.data   

    var _modificar = this.state.modificar;

    var _itemNombre = ""
    var _cantidad = ""
    var _valor = ""
    var _state = this.state
    var _links= ""
    if (_modificar) {
      _itemNombre = <input type="text" value={_state.itemNombre} onChange={this.itemNombreChange}/>
      _cantidad = <input type="text" value={_state.cantidad} onChange={this.cantidadChange}/>
      _valor = <input type="text" value={_state.valor} onChange={this.valorChange}/>
      _links = <div>
                <a href="#" onClick={this.cancel}>cancel</a> | 
                <a href="#"  onClick={this.update}>actualizar</a>
               </div>
    }
    else {
      _itemNombre = _item.item
      _cantidad = _item.cantidad
      _valor = _item.valor
      _links =  <div>
                  <a href="#" className="factura-link" onClick={this.modificarItem}>modificar</a>
                </div>
    }

    return  <div className="row-lista-detalles">
              <div>{_item.id}</div>
              <div>{_itemNombre}</div>
              <div>{_cantidad}</div>
              <div>{_valor}</div>
              <div>{_item.total}</div>
              {_links}
            </div>    
  }
});

module.exports = DetailRow;
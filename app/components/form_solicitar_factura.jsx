import React from 'react'

import uuidv1 from 'uuid/v1'
/*----------------------------------------*/
/* React Component Form Solicitar Factura */
/*----------------------------------------*/
var FormSolicitarFactura = React.createClass({

  getInitialState: function() {
      return({documento:"", nombre:"", email:""});
  },

  documentoChange: function(e) {
    var _value = e.target.value;
    this.setState({documento: _value});
  },
  nombreChange: function(e) {
    var _value = e.target.value;
    this.setState({nombre: _value});
  },
  emailChange: function(e) {
    var _value = e.target.value;
    this.setState({email: _value});
  },

  solicitarFactura: function() {
    //Iniciar proceso en BPM
    var _this = this    

    //var _bKey = Math.random().toString();
    const _bKey = uuidv1();
    fetch('http://127.0.0.1:8080/engine-rest/process-definition/key/factura/start', 
          {   method:"POST",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },

              body: JSON.stringify({ 
                      "businessKey": _bKey,
                      "variables": {
                        "documentoCliente": {"type": "String","value": _this.state.documento,"valueInfo": {}},
                        "nombreCliente": {"type": "String","value": _this.state.nombre,"valueInfo": {}},
                        "emailCliente": {"type": "String","value": _this.state.email,"valueInfo": {}},
                        "quejaRegistrada": {"type": "boolean","value": false,"valueInfo": {}},
                        "bKey": {"type": "String","value": _bKey,"valueInfo": {}},
                      }
                    })
            }
          )
    .then(function(response) { 
      console.log("ok proceso iniciado");
      return response.json()
    })
    .then(function(jsonObject) {
      console.log("jsonObject instancia del proceso")
      console.log(jsonObject)
    })
    .catch(function(err) {
        console.log("error iniciando proceso");
        console.log(err)
    }); 

  },

  render: function() {
    var _state = this.state;

    return  <div className="form-factura">
                <div>
                  <h3>Solicitar Factura</h3>
                </div>
                <div>
                  <input type="text" placeholder="documento" value={_state.documento} onChange={this.documentoChange} />
                </div>
                <div>
                  <input type="text" placeholder="nombre Cliente" value={_state.nombre} onChange={this.nombreChange} />
                </div>
                <div>
                  <input type="text" placeholder="Correo electrónico" value={_state.email} onChange={this.emailChange} />
                </div>

                <a href="#" onClick={this.solicitarFactura}>Solicitar Factura</a>
              </div>
  },

});

module.exports = FormSolicitarFactura;





import React from 'react'
import config from '../config/rest.js';
import { Table } from 'antd';
import moment  from 'moment';

/*----------------------------------------*/
/* React Component                        */
/*----------------------------------------*/
var FacturasXCrear = React.createClass({

  getInitialState: function() {
    return {datos:null, datosFactura:{documentoCliente:"-"}}    
  },

  datosFactura: function(e, taskInstanceId) {
    //trae las tareas crearFactura pendientes
    var _this = this;
    var _parent = this.props.parent;

    fetch(config.urls.taskVariables.replace(':taskInstanceId', taskInstanceId))
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      /*llega un array de facturas*/
      var _data = {
                    taskInstanceId: taskInstanceId,
                    documento: jsonObject.documentoCliente.value
                  };
      _parent.crearFactura(_data);

    })
    .catch(function(err) {
        console.log(err)
    }); 

  },

  getData: function() {
    //trae los tareas crearFactura pendientes
    var _this = this;
    var _parent = this.props.parent;
    var _rolUsuario = _parent.state.rolUsuario;

    fetch(config.urls.facturasXCrear.replace(':taskId', 'crearFactura').replace(':rol', _rolUsuario))
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      /*llega un array de facturas*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                fecha: item.created, 
                instanciaId: item.processInstanceId})
      });
      _this.setState({datos: _data});


    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  componentDidMount: function() {
    this.getData();
  },
  
  render: function() {
    var _datos = this.state.datos;
    var _items = "";
    var _this = this;

    const columns = [
    {
      title: 'Fecha',
      dataIndex: 'fecha',
      key: 'fecha',
      render: function(text) {return moment(text).format('DD/MM/YYYY')}
    },
    {
      title: 'Hora',
      dataIndex: 'fecha',
      key: 'hora',
      render: function(text) {return moment(text).format('HH:mm:ss')}
    },
    {
      title: 'taskInstanceId',
      dataIndex: 'id',
      key: 'taskInstanceId',
      render: function(text) {return text}
    },
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: function(text) {return <a href="#" onClick={(e) => _this.datosFactura(e, text)}>Crear Factura</a>}
    }
    ];

    if (_datos) {
      _items = <Table columns={columns} dataSource={_datos} />;
    }
    else {
      _items = <h2>cargando datos...</h2>
    }

    return <div>
            {_items}
           </div>
  },

});

module.exports = FacturasXCrear;
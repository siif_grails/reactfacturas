import React from 'react'
import config from '../config/rest.js';
import { Table } from 'antd';
import moment  from 'moment';

/*----------------------------------------*/
/* React Component Form Solicitar Factura */
/*----------------------------------------*/
var ClientesXCrear = React.createClass({

  getInitialState: function() {
    return {datos:null, datosCliente:{documento:"-", nombre:"-"}}    
  },

  datosCliente: function(e, taskInstanceId) {
console.log("taskInstanceId")    
console.log(taskInstanceId)    
    //trae los tareas crearCliente pendientes
    var _this = this;
    var _parent = this.props.parent;

    fetch(config.urls.taskVariables.replace(':taskInstanceId', taskInstanceId))
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      console.log(jsonObject)
      /*llega un array de facturas*/
      var _data = {
                    taskInstanceId: taskInstanceId,
                    documento: jsonObject.documentoCliente.value,
                    nombre: jsonObject.nombreCliente.value,
                    email: jsonObject.emailCliente.value
                  };
      _parent.crearCliente(_data);

    })
    .catch(function(err) {
        console.log(err)
    }); 

  },

  getData: function() {
    //trae los tareas crearCliente pendientes
    var _this = this;
    var _parent = this.props.parent;
    var _rolUsuario = _parent.state.rolUsuario;

    fetch(config.urls.clientesXCrear.replace(':taskId', 'crearCliente').replace(':rol', _rolUsuario))
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      console.log(jsonObject)
      /*llega un array de facturas*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                fecha: item.created, 
                instanciaId: item.processInstanceId})
      });
      _this.setState({datos: _data});


    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  componentDidMount: function() {
    this.getData();
  },
  
  render: function() {
    var _datos = this.state.datos;
    var _items = "";
    var _this = this;

    const columns = [
    {
      title: 'Fecha',
      dataIndex: 'fecha',
      key: 'fecha',
      render: function(text) {return moment(text).format('DD/MM/YYYY')}
    },
    {
      title: 'Hora',
      dataIndex: 'fecha',
      key: 'hora',
      render: function(text) {return moment(text).format('HH:mm:ss')}
    },
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: function(text) {return <a href="#" onClick={(e) => _this.datosCliente(e, text)}>Crear Cliente</a>}
    }
    ];

    if (_datos) {
      _items = <Table columns={columns} dataSource={_datos} />;
    }
    else {
      _items = <h2>cargando datos...</h2>
    }

    var _datosCliente = this.state.datosCliente.documento + " " + this.state.datosCliente.nombre
    return <div>
            {_items}
           </div>
  },

});

module.exports = ClientesXCrear;





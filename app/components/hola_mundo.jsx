import React from 'react'

import Resalt from './resaltado.jsx';

var HolaMundo = React.createClass({
  viejoMundoClick: function(e) {
  	this.refs.resaltado.setState({texto: "viejo mundo..."})
  },
  nuevoMundoClick: function(e) {
  	this.refs.resaltado.setState({texto: "nuevo mundo..."})
  },

  incrementar: function() {
  	this.refs.contador.incrementar();
  },
  decrementar: function() {
  	this.refs.contador.decrementar();
  },

  showRefs() {
  	this.refs.resaltado.showRefs()
  },

  render: function() {
    return 	<div>
    			Hola <Resalt ref="resaltado" texto="??" signo="?" parent={this}/>
    			<a href="#" onClick={this.viejoMundoClick}>viejo mundo</a> | 
    			<a href="#" onClick={this.nuevoMundoClick}>nuevo mundo</a>
    			<Contador ref="contador"/>
    			<a href="#" onClick={this.incrementar}>+ </a>
    			<a href="#" onClick={this.decrementar}>--</a>
    			<a href="#" onClick={this.showRefs}> ?</a>
    		</div>;
  }
});

var Contador = React.createClass({
  getInitialState: function() {
  	return({counter: 0})
  },

  incrementar: function() {
  	this.setState({counter: this.state.counter + 1 })
  },

  decrementar: function() {
  	var _counter = this.state.counter
  	if (_counter >0 ) { 
  		this.setState({counter: this.state.counter - 1 })
  	}
  },

  render: function() {
    return 	<div>{this.state.counter}</div>;
  }
});

module.exports = HolaMundo
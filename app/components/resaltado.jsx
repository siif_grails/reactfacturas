import React from 'react'
import styles from '../styles/estilos.css';

var Resaltado = React.createClass({
  getInitialState: function() {
  	return({texto: this.props.texto});
  },

  showRefs: function() {
  	console.log(this.props.parent.refs.resaltado.state);
  },

  render: function() {
  	var _texto = this.state.texto
  	var _signo = this.props.signo
    return <span className="resaltado">{_texto}{_signo}</span>
  }
});

module.exports = Resaltado
import React from 'react';
import { Layout, Menu, Icon } from 'antd';
const { Header, Sider, Content } = Layout;
import 'antd/dist/antd.css'; 
import styles from '../styles/estilos.css';
import ListaFacturas from './lista_facturas.jsx';
import SolicitarFactura from './form_solicitar_factura.jsx';
import ClientesXCrear from './clientes_x_crear.jsx';
import CrearCliente from './crear_cliente.jsx';
import FormNuevaFactura from './form_factura.jsx';
import ListaClientes from './lista_clientes.jsx';
import FacturasXCrear from './facturas_x_crear.jsx';
import Login from './login.jsx';



var LayoutFacturas = React.createClass({
  getInitialState: function(){
	 return {collapsed: false, opcion: 'verFacturas', rolUsuario:null, token: null}
  },
  toggle: function() {
    this.setState({ collapsed: !this.state.collapsed });
  },
  verFacturas: function(){
  	this.setState({opcion: 'verFacturas'});
  },
  verClientes: function(){
  	this.setState({opcion: 'verClientes'});
  },
  crearFactura: function(data = {}){
  	this.setState({opcion: 'crearFactura', crearFacturaData: data});
  },
  crearCliente: function(data){
    console.log("data");
    console.log(data);
  	this.setState({opcion: 'crearCliente', crearClienteData: data});
  },
  solicitarFactura: function(){
    this.setState({opcion: 'solicitarFactura'});
  },

  clientesXCrear: function(){
    this.setState({opcion: 'clientesXCrear'});
  },
  facturasXCrear: function(){
    this.setState({opcion: 'facturasXCrear'});
  },

  changeRol: function(e) {
    var _value = e.target.value;
    this.setState({rolUsuario: _value});
  },

  render: function() {
    var _render;
    if (this.state.opcion == 'verFacturas')
    	_render = <ListaFacturas parent={this}/>;

    if (this.state.opcion == 'solicitarFactura')
      _render = <SolicitarFactura />;

    if (this.state.opcion == 'clientesXCrear')
      _render = <ClientesXCrear parent={this} />;

    if (this.state.opcion == 'facturasXCrear')
      _render = <FacturasXCrear parent={this} />;

    if (this.state.opcion == 'crearCliente')
      _render = <CrearCliente data={this.state.crearClienteData}/>;

    if (this.state.opcion == 'verClientes')
      _render = <ListaClientes />;

    if (this.state.opcion == 'crearFactura')
      _render = <FormNuevaFactura modal="false" data={this.state.crearFacturaData}/>

    if (!this.state.token)
      _render = <Login parent={this} /> 

    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <div onClick={this.verFacturas}>
                <Icon type="copy"/>
                <span>Ver Facturas</span>
              </div>
            </Menu.Item>
            <Menu.Item key="2">
              <div onClick={this.crearFactura}>
                <Icon type="file-add"/>
                <span>Crear Factura</span>
              </div>
            </Menu.Item>
            <Menu.Item key="3">
              <div onClick={this.verClientes}>
                <Icon type="team"/>
                <span>Ver Clientes</span>
              </div>
            </Menu.Item>
            <Menu.Item key="4">
              <div onClick={this.crearCliente}>
	            <Icon type="user-add"/>
	            <span>Crear Cliente</span>
              </div>
            </Menu.Item>
            <Menu.Item key="5">
              <div onClick={this.solicitarFactura}>
              <Icon type="phone"/>
              <span>Solicitar Factura</span>
              </div>
            </Menu.Item>
            <Menu.Item key="6">
              <div onClick={this.clientesXCrear}>
              <Icon type="usergroup-add"/>
              <span>Clientes x Crear</span>
              </div>
            </Menu.Item>
            <Menu.Item key="7">
              <div onClick={this.facturasXCrear}>
              <Icon type="switcher"/>
              <span>Facturas x Crear</span>
              </div>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <select onChange={this.changeRol}>
              <option value="-1">Seleccione Rol</option>
              <option value="administrativa">Administrativo</option>
              <option value="cartera">Cartera</option>
              <option value="facturacion">Facturacion</option>
            </select>
          </Header>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
            {_render}
          </Content>
        </Layout>
      </Layout>
      );
  }
});

module.exports = LayoutFacturas;
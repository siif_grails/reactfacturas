import React from 'react'
import { Table, Modal, Button } from 'antd';

import DetailRow from './detail_row';
import config from '../config/rest.js';


var ItemsFactura =  React.createClass({
  getInitialState: function() {
    var _data = this.props.data;

    return({verDetalles: false, datos:[]       
    });
  },

  verDetalles: function(e, idFactura) {
    //traer la factura con la API rest del id de factura correspondiente
    /*var _verDetalles = this.state.verDetalles;

    if (!_verDetalles) {
      if (!this.state.datos.length>0) {
        this.getData();  
      }
      else {
        this.setState({verDetalles:true})
      }
    }
    else {
     this.ocultarDetalles()
    }*/
    //this.setState({datos: [], idFactura: idFactura});
    this.setState({verDetalles: false});
    this.getData(idFactura);
  },

  getData: function(idFactura) {
    //trae el registros de la base de datos
    var _this = this    
    fetch(config.urls.items.replace(':id', idFactura))  
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {
      console.log(jsonObject)
      /*llega un array de items*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id,
                nombre: item.item,
                cantidad: item.cantidad,
                valor: item.valor
              })
      });
      _this.setState({verDetalles: true, datos: _data, visible: true});

    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  ocultarDetalles: function() {
    this.setState({verDetalles:false});
  },


  showModal: function (){
    this.setState({
      visible: true,
      verDetalle: true
    });
  },

  handleOk: function (e){
    console.log(e);
    this.setState({
      visible: false,
    });
  },

  handleCancel: function(e){
    console.log(e);
    this.setState({
      visible: false,
    });
  },
 
  render: function() {
    var _this = this;
    var _item = this.props.data

    var _rows = ""
    var _ocultar = ""
    var _detalle = ""
    var _datos = this.state.datos;

    var _factura = ""
    var _state = this.state 
    var _modificar = _state.modificar

//   this.verDetalles;


    if (this.state.verDetalles) {
      console.log('Ver detalles state: ' + this.state.verDetalles);
      _rows = _datos.map(function(item,i){
        return <DetailRow key={"dr_"+item.id } ref={"dr_"+item.id } data={item} parent={_this}/>
        //return <div>{item.nombre}</div>;
      });
    } 

    return      <div>
                    <a href="#" onClick={(e) => this.verDetalles(e, _this.props.idFactura)}>Ver Detalles</a>
                  <Modal
                    title="Detalle Factura"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    okText = "OK"
                    cancelText = "Cancelar"
                  >
                    {_rows}
                  </Modal>



                </div>
  }
});

module.exports = ItemsFactura;
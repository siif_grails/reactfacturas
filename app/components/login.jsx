import React from 'react'
import styles from '../styles/estilos.css';
import config from '../config/rest.js';

import { Pagination, LocaleProvider } from 'antd'
import esES from 'antd/lib/locale-provider/es_ES'

var Login = React.createClass({
  getInitialState: function() {
  	return ({user:"", password:""});
  },

 userChange: function(e) {
    var _value = e.target.value;
    this.setState({user: _value});
  },  

 passwordChange: function(e) {
    var _value = e.target.value;
    this.setState({password: _value});
  },  

  login: function(e) {
  	var _state = this.state;
    var _parent = this.props.parent;

    fetch(config.urls.login, 
          {   method:"POST",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({username: _state.user, password: _state.password})
            }
          )
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {
        var _respuesta = jsonObject;
        if (_respuesta.ok) {
          _parent.setState({token: _respuesta.token})
        }
    })
    .catch(function(err) {
		console.log("Error en login")
        console.log(err)
    }); 

  },  

  render: function() {
  	var _state = this.state;
    return 	<div className="detalles-container">
    			<h1>Login Facturas</h1>
    			<div>
    				<span>user:</span>
    				<span>
    					<input type="text" value={_state.user} onChange={this.userChange}/>
    				</span>
    			</div>
    			<div>
    				<span>Password</span>
    				<span>
    					<input type="password" value={_state.password} onChange={this.passwordChange}/>
    				</span>
    			</div>
				<a href="#" onClick={this.login}>login</a> 
    		</div>;
  }
});

module.exports = Login
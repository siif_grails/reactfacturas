import React from 'react'

import DetailRow from './detail_row';
import config from '../config/rest.js';


var ListRow =  React.createClass({
  getInitialState: function() {
    var _data = this.props.data;

    return({verDetalles: false, datos:[], modificar:false,
            documento: _data.documento,
            nombreCliente: _data.cliente,
            fecha: _data.fecha,
            numero: _data.numero
    });
  },

  verDetalles: function() {
    //traer la factura con la API rest del id de factura correspondiente
    var _verDetalles = this.state.verDetalles;

    if (!_verDetalles) {
      if (!this.state.datos.length>0) {
        this.getData();  
      }
      else {
        this.setState({verDetalles:true})
      }
    }
    else {
     this.ocultarDetalles()
    }
  },

  modificar: function() {
    var _modificar = !this.state.modificar;
    this.setState({modificar:_modificar})
  },

  getData: function() {
    //trae el registros de la base de datos
    var _this = this    
    fetch(config.urls.items.replace(':id', this.props.data.id))  
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {
      console.log(jsonObject)
      /*llega un array de items*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                item: item.item,
                cantidad: item.cantidad, 
                valor: item.valor, 
                total:item.cantidad * item.valor
              })
      });
      _this.setState({verDetalles: true, datos: _data});

    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  ocultarDetalles: function() {
    this.setState({verDetalles:false});
  },

  documentoChange: function(e) {
    var _value = e.target.value;
    this.setState({documento: _value});
  },
  nombreClienteChange: function(e) {
    var _value = e.target.value;
    this.setState({nombreCliente: _value});
  },
  fechaChange: function(e) {
    var _value = e.target.value;
    this.setState({fecha: _value});
  },
  numeroChange: function(e) {
    var _value = e.target.value;
    this.setState({numero: _value});
  },

  render: function() {
    var _item = this.props.data

    var _rows = ""
    var _ocultar = ""
    var _detalle = ""
    var _datos = this.state.datos;

    var _factura = ""
    var _state = this.state 
    var _modificar = _state.modificar

    if (_modificar) {
      _factura =  <div className="row-lista">
                    <div>{_item.id}</div>
                    <div>
                      <input type="text" placeholder="documento" value={_state.documento} onChange={this.documentoChange} /> 
                      <input type="text" placeholder="nombre" value={_state.nombreCliente} onChange={this.nombreClienteChange} /> 
                    </div>
                    <div><input type="text" value={_state.fecha} onChange={this.fechaChange} /></div>
                    <div><input type="text" value={_state.numero} onChange={this.numeroChange} /></div>
                    <div>{_item.valor}</div>
                    <div>
                      <a href="#" className="factura-link" onClick={this.verDetalles}>detalles</a> | 
                      <a href="#" className="factura-link" onClick={this.modificar}>modificar</a>
                    </div>
                  </div>
    }
    else {
      _factura =  <div className="row-lista">
                    <div>{_item.id}</div>
                    <div>{_item.cliente}</div>
                    <div>{_item.fecha}</div>
                    <div>{_item.numero}</div>
                    <div>{_item.valor}</div>
                    <div>
                      <a href="#" className="factura-link" onClick={this.verDetalles}>detalles</a> | 
                      <a href="#" className="factura-link" onClick={this.modificar}>modificar</a>
                    </div>
                  </div>
    }


    if (this.state.verDetalles) {
      var _this = this
      _rows = _datos.map(function(item,i){
        return <DetailRow key={"dr_"+item.id } ref={"dr_"+item.id } data={item} parent={_this}/>
      });
    } 
    return      <div>
                  {_factura}
                  {_rows}
                </div>
  }
});

module.exports = ListRow;
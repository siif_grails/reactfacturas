import React from 'react';
import { message, Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, DatePicker, Modal } from 'antd';
import moment from 'moment';
import config from '../config/rest.js';

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;

const FacturaCreateForm = Form.create()(
  (props) => {
    const { form, renderClientes } = props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };   


    return ( 
        <Form orientation="vertical">
          <FormItem
                {...formItemLayout}
                  label="Número"
                  hasFeedback
              >
                    {getFieldDecorator('numero', {
                  rules: [
                      { required: true, message: 'Por favor indique el número de la factura' },
                  ],
                  })(
              <Input placeholder="Número"/>                 
            )}
          </FormItem>
          <FormItem
                {...formItemLayout}
                  label="Cliente"
                  hasFeedback
              >
                    {getFieldDecorator('cliente', {
                  rules: [
                      { required: true, message: 'Por favor seleccione el cliente!' },
                  ],
                  })(
              <Select placeholder="Por favor seleccione un cliente"
                  showSearch
                  style={{ width: 200 }}
                  optionFilterProp="children"
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {renderClientes()}
              </Select>
                  )}
          </FormItem>

          <FormItem
                {...formItemLayout}
                  label="Fecha"
                  hasFeedback
              >
                    {getFieldDecorator('fecha', {
                  rules: [
                      { required: false, message: 'Por favor seleccione fecha y hora' },
                  ],
                  })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="seleccione Fecha y hora"
              />
                  )}
          </FormItem>

        </Form>
        );
  }
);

var FormFactura = React.createClass({
  getInitialState: function(){
    return  { visible: false, clientes:[] };
  },
  showModal: function() {
    this.setState({ visible: true });
  },
  handleCancel: function() {
    this.setState({ visible: false });
  },
  handleCreate: function() {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

    var _this = this    
    var _factura = JSON.stringify({
                documento: values.cliente, 
                numero: values.numero,
                fecha: moment(values.fecha)
                })
    fetch(config.urls.facturas, 
          {   method:"POST",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: _factura
            }
          )
    .then(function(response) { 
      console.log("ok factura insertada");
      return response.json()
    })
    .then(function(jsonObject) {
      console.log(jsonObject);
      console.log(_this.props.modal);
      console.log("taskInstanceId");
      console.log(_this.props.data.taskInstanceId);

        /*Finalizar la tarea de crearCliente en camunda*/
        fetch(config.urls.taskComplete.replace(":taskInstanceId", _this.props.data.taskInstanceId), 
              {   method:"POST",
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },

                  body: JSON.stringify({})
                }
              )
        .then(function(response) { 
          console.log("===> Tarea de camunda crear factura finalizada");
          if (_this.props.modal === "true"){
            var _parent = _this.props.parent;
            _parent.getData();
          } else {
            message.success('Se creó correctamente la factura ');
          }          
        })
        .catch(function(err) {
            console.log("error finalizando tarea crear factura en camunda");
            console.log(err)
        }); 

    })
    .catch(function(err) {
        message.error(err);
        console.log("error updating");
        console.log(err)
    });       

      form.resetFields();
      this.setState({ visible: false });
  })
  },
  saveFormRef: function(form) {
    this.form = form;
  },
  getData: function() {
    //trae los registros de clientes
    var _this = this    
    fetch(config.urls.clientes)
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      console.log(jsonObject)
      /*llega un array de clientes*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                documento: item.documento, 
                nombre: item.nombre
                })
      });
      _this.setState({clientes: _data});

    })
    .catch(function(err) {
        console.log(err)
    }); 
  },  

  componentDidMount: function() {
    this.getData();
  },

  renderClientes: function(){
    if (this.state.clientes.length > 0) {
    return this.state.clientes.map(function(item,i) {
          return <Option value={item.documento} key={item.id}>{item.nombre}</Option>;
        });
    }

  },

  render: function() {
    var formulario;
    if (this.props.modal === "true"){
      formulario =
          <div>
            <Button type="primary" onClick={this.showModal}>Nueva Factura</Button>
            <Modal
              visible={this.state.visible}
              title="Crear nueva factura"
              okText="Crear"
              onCancel={this.handleCancel}
              onOk={this.handleCreate}
            >
              <FacturaCreateForm
                ref={this.saveFormRef}
                renderClientes={this.renderClientes}
              />
            </Modal>
          </div>;
    } else {
      formulario =               
          <div>
            <FacturaCreateForm
                ref={this.saveFormRef}
                renderClientes={this.renderClientes}
            />
            <Button type="primary" onClick={this.handleCreate}>Guardar</Button>
          </div>
    }
    return (
      <div>
        {formulario}
      </div>
    );
  }
});

module.exports = FormFactura;
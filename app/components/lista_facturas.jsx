import React from 'react'
import styles from '../styles/estilos.css';

import { Table, Modal, Button } from 'antd';

import Encabezado from './encabezado';
import Detalles from './detalles';
import ListRow from './list_row';
import ItemsFactura from './items_factura';
import EditarFactura from './editarFactura.jsx';

import moment  from 'moment';
import config from '../config/rest.js';


import FormNuevaFactura from './form_factura.jsx';

//Formatea un valor n en una cadena separada por comas y con moneda currency
//scale es la escala de valor: 1, 1000, 1000000, etc
function formatCurrency(n, currency, scale) {
    return currency + " " + (n/scale).toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

var ListaFacturas = React.createClass({
  getInitialState: function() {
    return {
            datos:[], 
            nuevaFactura:false, 
            solicitarFactura:false, 
            idFacturaEditar:"", 
            loading: false, 
            visible: false}    
  },

  getData: function() {
    //trae los registros de la base de datos
    var _this = this;
    var _parent = this.props.parent;
    console.log(config);    
    fetch(config.urls.facturas,
                  {   method:"GET",
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+ _parent.state.token
                      }
                  }
      )
    .then(function(response) { 
      return response.json()
    })
    .then(function(jsonObject) {

      console.log(jsonObject)
      /*llega un array de facturas*/
      var _data = jsonObject.map(function(item,i){
        return({id: item.id, 
                documento: item.cliente.documento, 
                cliente: item.cliente.nombre, 
                fecha: item.fecha, 
                numero:item.numero,
                valor: item.valor})
      });
      _this.setState({datos: _data});


    })
    .catch(function(err) {
        console.log(err)
    }); 
  },

  componentDidMount: function() {
    this.getData();
  },

  btnclick: function() {
    console.log("hijos")
    console.log(this.refs.item_1);
  },

  nuevaFactura: function(){
    this.setState({nuevaFactura: true});
    if (this.refs.facturaModal)
      this.refs.facturaModal.showModal();
  },

  showModal: function(e, idFactura){
    this.setState({
      visible: true,
      idFacturaEditar: idFactura
    });
  },
  
  handleOk: function(){
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  },

  handleCancel: function(){
    this.setState({ visible: false });
  },

  //para factura modal
  handleCreate: function() {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log('Received values of form: ', values);
      form.resetFields();
      this.setState({ visible: false });
    });
  },  

  render: function() {
    var _datos = this.state.datos;
    var _items = "";
    var _this = this;

    const columns = [{
      title: 'Numero',
      dataIndex: 'numero',
      key: 'numero',
      render: function(text, record) {return <a href="#" onClick={(e) => _this.showModal(e, record.id)}>{text}</a>},
    }, {
      title: 'Fecha',
      dataIndex: 'fecha',
      key: 'fecha',
      render: function(text) {return moment(text).format('DD/MM/YYYY HH:mm:ss') },
    }, {
      title: 'Valor',
      dataIndex: 'valor',
      key: 'valor',
      className: 'ralign',
      render: function(text){ return formatCurrency(text, '$', 1); },
    }, {
      title: 'Documento',
      dataIndex: 'documento',
      key: 'cliente.documento'
    }, {
      title: 'Cliente',
      dataIndex: 'cliente',
      key: 'cliente',
    }, {
     title: ' ',
     dataIndex: 'id',
     key: 'id',
     render: function(text) {return <ItemsFactura idFactura={text} />},
    }];

    if (_datos.length > 0) {
      _items = <Table columns={columns} dataSource={_datos} />;
    }
    else {
      _items = <h2>cargando datos...</h2>
    }

    var _formFactura = ""

    if (this.state.solicitarFactura) {
      _formFactura = <FormSolicitarFactura parent={this}/>
    }

    if (this.state.idFacturaEditar != "") {
      _formFactura =  <Modal
                        visible={this.state.visible}
                        title="Editar Factura"
                        footer={[
                          <Button key="back" size="large" onClick={this.handleCancel}>Cancelar</Button>,
                          <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleOk}>
                            Guardar
                          </Button>,
                        ]}          
                      >
                        <EditarFactura ref="editarFacturaRef" parent={this} idFactura={this.state.idFacturaEditar} />
                      </Modal>
    }

    return <div>
            {_items}

            {_formFactura}
            <FormNuevaFactura parent={this} modal="true"/>
        </div>
  }

});

module.exports = ListaFacturas
import React from 'react'
import styles from '../styles/estilos.css';

import Encabezado from './encabezado';
import Detalles from './detalles';
import logo from '../images/logo.png';


var Factura = React.createClass({

  render: function() {
    return 	<div className="factura-container">
    			<Banner />
    			<Encabezado />
    			<Detalles />
    		</div>;
  }
});

var Banner = React.createClass({

  render: function() {
    return 	<div className="banner">
    			<img src={logo} />
    		</div>;
  }
});

module.exports = Factura
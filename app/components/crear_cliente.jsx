import React from 'react'
import { Input, Button, AutoComplete, Alert} from 'antd';
import config from '../config/rest.js';

/*----------------------------------------*/
/* React Component Form Crear Cliente     */
/*----------------------------------------*/
var FormCrearCliente = React.createClass({

  getInitialState: function() {
    var _data = null;
    if (this.props && this.props.data) {
      _data = this.props.data;
    }

    var _documento = null
    if (_data && _data.documento) {
      _documento = _data.documento;
    }
    var _nombre = null
    if (_data && _data.nombre) {
      _nombre = _data.nombre;
    }

    var _email = null
    if (_data && _data.email) {
      _email = _data.email;
    }

    return({documento:_documento, nombre:_nombre, dataSource:[], telefono:null, email:_email, direccion:null, mostrarAlerta:false, deshabilitado:false, data:_data});
  },
  documentoChange: function(e) {
    var _value = e.target.value;
    this.setState({documento: _value});
  },
  nombreChange: function(e) {
    var _value = e.target.value;
    this.setState({nombre: _value});
  },
  telefonoChange: function(e) {
    var _value = e.target.value;
    this.setState({telefono: _value});
  },
  emailChange: function(e) {
    var _value = e;

    this.setState({
      dataSource: !_value || _value.indexOf('@') >= 0 ? [] : [
        `${_value}@gmail.com`,
        `${_value}@hotmail.com`,
        `${_value}@solinfo.com.co`,
      ],
    });
    this.setState({email: _value});
  },
  direccionChange: function(e) {
    var _value = e.target.value;
    this.setState({direccion: _value});
  },

  crearCliente: function() {
    var _this = this    

    fetch(config.urls.clientes, 
          {   method:"POST",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },

              body: JSON.stringify({ "documento": _this.state.documento,
                                     "nombre":    _this.state.nombre,
                                     "telefono":  _this.state.telefono,
                                     "email":     _this.state.email,
                                     "direccion": _this.state.direccion
                    })
            }
          )
    .then(function(response) { 
        /*Finalizar la tarea de crearCliente en camunda*/
        fetch(config.urls.taskComplete.replace(":taskInstanceId", _this.props.data.taskInstanceId), 
              {   method:"POST",
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },

                  body: JSON.stringify({})
                }
              )
        .then(function(response) { 
          console.log("===> Tarea de camunda finalizada");
          console.log("===> Cliente Creado");
          _this.setState({mostrarAlerta:true, deshabilitado:true}); 
          
        })
        .catch(function(err) {
            console.log("error finalizando tarea crear cliente en camunda");
            console.log(err)
        }); 
    })
    .catch(function(err) {
        console.log("error creando cliente");
        console.log(err)
    }); 

  },

  render: function() {
    var _state = this.state;

    return  <div>
                <div>
                  <h3>Ingrese los datos del Cliente</h3>
                </div>
                <div>
                  <Input placeholder="documento" 
                         value={_state.documento} 
                         onChange={this.documentoChange} 
                         style={{ width: 400 }} 
                         disabled={_state.deshabilitado} />
                </div>
                <div>
                  <Input placeholder="nombre Cliente" 
                         value={_state.nombre} 
                         onChange={this.nombreChange} 
                         style={{ width: 400 }} 
                         disabled={_state.deshabilitado} />
                </div>
                <div>
                  <Input placeholder="teléfono" 
                         value={_state.telefono} 
                         onChange={this.telefonoChange} 
                         style={{ width: 400 }} 
                         disabled={_state.deshabilitado} />
                </div>
                <div>
                  <AutoComplete
                    dataSource={_state.dataSource}
                    style={{ width: 400 }} 
                    onChange={this.emailChange}
                    value={_state.email}
                    placeholder="Email"
                    disabled={_state.deshabilitado}
                  />
                </div>
                <div>
                  <Input placeholder="dirección" 
                         value={_state.direccion} 
                         onChange={this.direccionChange} 
                         style={{ width: 400 }} 
                         disabled={_state.deshabilitado} />
                </div>
                <div>
                  <Button type="primary" 
                          onClick={this.crearCliente}
                          disabled={_state.deshabilitado}>
                          Crear Cliente</Button>
                </div>
                {_state.mostrarAlerta?<div style={{ width: 400 }}><Alert message="Cliente Creado" type="success" /></div>:""}
              </div>
  },

});

module.exports = FormCrearCliente;




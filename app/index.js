import React from 'react'
import ReactDOM from 'react-dom'
import { Pagination, LocaleProvider } from 'antd'
import esES from 'antd/lib/locale-provider/es_ES'

import LayoutFacturas from './components/layout_facturas'


ReactDOM.render(
	<LocaleProvider locale={esES}>
    	<LayoutFacturas />
  	</LocaleProvider>, document.getElementById("contenido"));

const webpack = require('webpack');
const path = require('path'); 
const PATHS = { 
    app: path.join(__dirname, 'app'), 
    build: path.join(__dirname, 'build'),
    images: path.join(__dirname, 'app/images'),	
    images_widgets: path.join(__dirname, 'node_modules/react-widgets')  
};
module.exports = {
  resolve: {
    extensions: ['.js', '.jsx','.png', '.jpg']
  },  	
  entry: './app/index.js',
  output: { path: PATHS.build, filename: 'bundle.js'},
  devServer:{
   contentBase: PATHS.build,
   inline : true,
   hot:true,
   stats: 'errors-only',  
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
 /*for react widgets*/
    { test: /\.less$/, loaders: ['style-loader', 'css-loader', 'less-loader'] },
    { test: /(\.css|\.scss)$/, loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']},
	  { test: /\.(jpg|png|gif)$/, loaders: ['url-loader?limit=10000&name=./images/[hash].[ext]']},
	  { test: /\.jsx?$/,loaders: ['babel-loader?presets[]=es2015&presets[]=react'], include: PATHS.app },
    { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loaders: ['url-loader?limit=100000'] }
    ]
  },  
}
